import React from 'react'
import './styles.css';

const sitesAboutSecretSanta = [
  "https://en.wikipedia.org/wiki/Secret_Santa",
  "https://www.notonthehighstreet.com/christmas/secret-santa-gifts",
  "https://www.goodnewsnetwork.org/tyler-perry-pays-off-430k-of-walmart-layways/",
  "https://5newsonline.com/2018/12/07/secret-santa-pays-off-27k-in-walmart-layaways-in-clarksville/"
];

const About = () => (
  <div className={'about-container'}>
    <h1>About Secret Santa</h1>
    <p>Want to learn more about Secret Santa? Visit the following sites:</p>
    <ul>
      {sitesAboutSecretSanta.map((site, index) => <li key={site + '_' + index}><a href={site}>{site}</a></li>)}
    </ul>
  </div>
)

export default About
