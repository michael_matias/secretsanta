import React from 'react'
import AppBar from '@material-ui/core/AppBar';
import Button from '@material-ui/core/Button';
import { Link } from 'react-router-dom';
import Toolbar from '@material-ui/core/Toolbar';

class Header extends React.Component {
  render() {
    return (
      <div>
        <AppBar position="static" color={'default'}>
          <Toolbar>
            <Button color="inherit"><Link to="/">Home</Link></Button>
            <Button color="inherit"><Link to="/about-us">About</Link></Button>
          </Toolbar>
        </AppBar>
      </div>
    )
  }
}

export default Header;