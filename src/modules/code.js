export const SUBMIT_CODE = 'SUBMIT_CODE';
export const SANTA_RESULT_REQUESTED = 'SANTA_RESULT_REQUESTED';
export const GET_USER_ASSIGNED_PERSON = 'GET_USER_ASSIGNED_PERSON';
export const SET_ERROR_MESSAGE = 'SET_ERROR_MESSAGE';
const axios = require('axios');

const api = process.env.NODE_ENV === 'development' ? 'http://localhost:3000/' : 'https://matias-secret-santa-api.herokuapp.com/';

console.log(process.env.NODE_ENV);

const initialState = {
  result: {},
  isGettingSantaResult: false,
  userAlreadyAssignedPerson: false,
  errorMessage: undefined
};

export default (state=initialState, action) => {
  switch (action.type) {
    case SANTA_RESULT_REQUESTED:
      return {
        ...state,
        isGettingSantaResult: true
      };
    case SUBMIT_CODE:
      return {
        ...state,
        result: Object.assign({}, action.result, {dataRetrieved: true}),
        isGettingSantaResult: !state.isGettingSantaResult
      };
    case GET_USER_ASSIGNED_PERSON:
      return {
        ...state,
        userAlreadyAssignedPerson: action.result
      };
    case SET_ERROR_MESSAGE:
      return {
        ...state,
        errorMessage: action.message
      };
    default:
      return state;
  }
}

export const submitCode = (code) => {
  return dispatch => {
    dispatch({
      type: SANTA_RESULT_REQUESTED
    });
    setTimeout(() => {
      axios(api + 'secretSantaPerson', {
        method: 'POST',
        data: {
          code,
        },
        withCredentials: true
      })
        .then(result => result.data)
        .then(result => {
          if (result.result) {
            dispatch({
              type: SUBMIT_CODE,
              result: result.result
            })
          } else {
            dispatch({
              type: SET_ERROR_MESSAGE,
              message: result.message
            })
          }
        });
    }, 1500);
  }
};

export const getUserAssignedPerson = () => {
  return dispatch => {
    axios(api + 'alreadyAssignedPerson', {
      method: 'GET',
      withCredentials: true
    })
      .then(result => result.data)
      .then(result => {
        dispatch({
          type: GET_USER_ASSIGNED_PERSON,
          result
        })
      });
  }
};
