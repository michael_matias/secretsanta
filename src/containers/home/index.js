import React from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { getUserAssignedPerson } from '../../modules/code'
import './styles.css'
import Santa from './../../assets/images/santa.jpg'
import CircularProgress from '@material-ui/core/CircularProgress/CircularProgress'
import SantaResult from './../santa-result/index';
import CodeInput from './../code-input/index';
import GetIdeas from './../get-ideas/index';
import Grid from '@material-ui/core/Grid';


class Home extends React.Component {
  constructor(props) {
    super(props);
    this.initHome();
  }

  initHome = () => {
    this.props.getUserAssignedPerson();
  };

  render() {
    return (
      <div className={'home-container'}>
        <Grid container
              justify="center"
              alignItems="center"
              direction={"column"}
              spacing={0}>
          <Grid item>
            <h1 className={'title'}>Welcome to Secret Santa</h1>
          </Grid>
          <Grid item xs={12}>
            <p className={'subtitle'}>The Jewish Kind</p>
          </Grid>
          <Grid container
                justify="center"
                alignItems="center"
                direction={"column"}>
              {this.props.errorMessage &&
                <Grid item>
                  <div>{this.props.errorMessage}</div>
                </Grid>
              }
              {!this.props.errorMessage &&
                <Grid container
                      justify="center"
                      alignItems="center"
                      direction={"column"}>
                  {!this.props.result.dataRetrieved &&
                  <Grid item>
                    <img src={Santa} alt={'santa'} className={'santa-image'}/>
                  </Grid>
                  }
                  {!this.props.result.dataRetrieved && !this.props.isGettingSantaResult &&
                    <Grid item>
                      <CodeInput/>
                    </Grid>
                  }
                  {this.props.isGettingSantaResult &&
                    <Grid item>
                      <CircularProgress />
                    </Grid>
                  }
                  {this.props.result.dataRetrieved &&
                  <Grid item>
                    <div className={'santa-result-container'}>
                      <SantaResult/>
                      <GetIdeas/>
                    </div>
                  </Grid>
                  }
              </Grid>
              }
            </Grid>
          </Grid>
      </div>
    )
  }
}

const mapStateToProps = ({code}) => ({
  result: code.result,
  isGettingSantaResult: code.isGettingSantaResult,
  userAlreadyAssignedPerson: code.userAlreadyAssignedPerson,
  errorMessage: code.errorMessage
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getUserAssignedPerson
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home)
