import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import connect from 'react-redux/es/connect/connect'
import { SocialIcon } from 'react-social-icons'

const styles = {
  actions: {
    display: 'flex',
    justifyContent: 'center'
  },
  card: {
    maxWidth: 345,
  },
  media: {
    height: 200,
  },
};

function MediaCard(props) {
  const { classes } = props;
  return (
    <Card className={classes.card}>
      <CardActionArea>
        <CardMedia
          className={classes.media}
          image={props.result.imageUrl}
          title="Contemplative Reptile"
        />
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2" align={'center'}>
            {props.result.name}
          </Typography>
          <ul>
            {props.result && props.result.interests.map((interest, index) => <li key={interest + '_' + index}>{interest}</li>)}
          </ul>
        </CardContent>
      </CardActionArea>
      <CardActions className={classes.actions}>
        <SocialIcon url={props.result.facebook} />
        <SocialIcon url={props.result.instagram} />
      </CardActions>
    </Card>
  );
}

MediaCard.propTypes = {
  classes: PropTypes.object.isRequired,
};

const mapStateToProps = ({code}) => ({
  result: code.result
});

export default connect(mapStateToProps, null)(withStyles(styles)(MediaCard));
