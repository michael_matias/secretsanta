import Button from '@material-ui/core/Button/Button'
import React from 'react';
import connect from 'react-redux/es/connect/connect'
import { bindActionCreators } from 'redux'
import { submitCode } from '../../modules/code'

class CodeInput extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      code: ''
    }
    this.handleCodeChange = this.handleCodeChange.bind(this);
    this.shouldSubmitBeDisabled = this.shouldSubmitBeDisabled.bind(this);
  }

  handleCodeChange = (event) => {
    this.setState({
      code: event.target.value
    })
  }

  submitCode = () => {
    this.props.submitCode(this.state.code);
  }

  shouldSubmitBeDisabled = () => {
    return this.state.code === '';
  }

  render() {
    return (
      <div className={'code-input'}>
        <input value={this.state.code} placeholder={'Enter your code'} onChange={this.handleCodeChange}/>
        <Button variant="contained" color="primary" onClick={this.submitCode} disabled={this.shouldSubmitBeDisabled()}>I'm ready to know my partner!</Button>
      </div>
    )
  }
}

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      submitCode
    },
    dispatch
  )

export default connect(null, mapDispatchToProps)(CodeInput);
